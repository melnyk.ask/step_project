// 1_JS
let web_design = document.querySelector(".web_design");
let graphic_design = document.querySelector(".graphic_design");
let online_support = document.querySelector(".online_support");
let app_design = document.querySelector(".app_design");
let online_marketing = document.querySelector(".online_marketing");
let seo_service = document.querySelector(".seo_service");

let web_design_text = document.querySelector(".web_design_text");
let graphic_design_text = document.querySelector(".graphic_design_text");
let online_support_text = document.querySelector(".online_support_text");
let app_design_text = document.querySelector(".app_design_text");
let online_marketing_text = document.querySelector(".online_marketing_text");
let seo_service_text = document.querySelector(".seo_service_text");

let our_services_info_img = document.querySelector(".our_services_info_img");
let our_services_info_text = document.querySelector(".our_services_info_text");

// 2_JS
let our_amazing_imgs = document.querySelector(".our_amazing_imgs");
let our_amazing_btn = document.querySelector(".our_amazing_btn");
let container3 = document.querySelector(".container3");
let our_amazing = document.querySelector(".our_amazing");
let our_amazing2 = document.querySelector(".our_amazing2");


// 3_JS
let our_amazing_menu_btn1 = document.querySelector(".our_amazing_menu_btn1");
let our_amazing_menu_btn2 = document.querySelector(".our_amazing_menu_btn2");
let our_amazing_menu_btn3 = document.querySelector(".our_amazing_menu_btn3");
let our_amazing_menu_btn4 = document.querySelector(".our_amazing_menu_btn4");
let our_amazing_menu_btn5 = document.querySelector(".our_amazing_menu_btn5");
let our_amazing_menu_btn1_text = document.querySelector(".our_amazing_menu_btn1_text");
let our_amazing_menu_btn2_text = document.querySelector(".our_amazing_menu_btn2_text");
let our_amazing_menu_btn3_text = document.querySelector(".our_amazing_menu_btn3_text");
let our_amazing_menu_btn4_text= document.querySelector(".our_amazing_menu_btn4_text");
let our_amazing_menu_btn5_text = document.querySelector(".our_amazing_menu_btn5_text");

// 4_JS
let slider_text = document.querySelector(".slider_text");
let slider_name = document.querySelector(".slider_name");
let slider_position = document.querySelector(".slider_position");
let slider_texts = []; //["ANNA COOK", "STEPAN FIGA", "HASAN ALI", "INGRID BULLET"];
let slider_names = []; 
let slider_positions = []; 
let slider_main_photo = document.querySelector(".slider_main_photo");
let slider_photos_btnleft = document.querySelector(".slider_photos_btnleft");
let slider_photos_btnright = document.querySelector(".slider_photos_btnright");
let slider_photos_container = document.querySelector(".slider_photos_container");
let slider_photos_1 = document.querySelector(".slider_photos_1");
let slider_photos_2 = document.querySelector(".slider_photos_2");
let slider_photos_3 = document.querySelector(".slider_photos_3");
let slider_photos_4 = document.querySelector(".slider_photos_4");
let slider_photos_5 = document.querySelector(".slider_photos_5");
let slider_photos_6 = document.querySelector(".slider_photos_6");
let slider_photos_7 = document.querySelector(".slider_photos_7");
let slider_photos_8 = document.querySelector(".slider_photos_8");
let slider_photos_9 = document.querySelector(".slider_photos_9");
let slider_photos_10 = document.querySelector(".slider_photos_10");
let slider_photos_11 = document.querySelector(".slider_photos_11");
let slider_photos_12 = document.querySelector(".slider_photos_12");

for (let i = 0; i <= 2; i++) { 
    slider_texts[i * 4] = `
    After the shooting of Captain McCluskey, the police took revenge on all 
    five New York families. The Five Families War of 1946 had begun. But 
    Michael wasn't there. He was hiding thousands of miles away, in Sicily. 
    He was staying with Don Tommasino.`;
    slider_texts[i * 4 + 1] = `
    In the evenings, Michael sat in a huge garden filled 
    with flowers, drinking wine and hearing old stories about his family. 
    During the day, he walked in the Sicilian countryside, dressed in old 
    clothes. Two bodyguards, Fabrizio and Calo. `;
    slider_texts[i * 4 + 2] = `
    Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar
    odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi
    pulvinar odio eget aliquam facilisis. `;
    slider_texts[i * 4 + 3] = `
    One morning, seven months after arriving in Sicily, Michael decided to 
    walk into the mountains past the hilltop town of Corleone. He walked 
    with his two bodyguards along dusty country roads, past fruit-trees and 
    fields of flowers. The hot, still air was rich with the smell of oranges.`;    

    slider_names[i*4] = "ANNA COOK";
    slider_names[i*4 + 1] = "STEPAN FIGA";
    slider_names[i*4 + 2] = "HASAN ALI";
    slider_names[i * 4 + 3] = "INGRID BULLET";

    slider_positions[i*4] = "Front-end Designer";
    slider_positions[i*4 + 1] = "Engineer";
    slider_positions[i*4 + 2] = "UX Designer";
    slider_positions[i * 4 + 3] = "Driver";   
}

console.log(slider_names);
// start 1_JS
web_design.onclick = () => {
    web_design.classList.add("our_services_rest_pressed");
    web_design_text.classList.add("our_services_rest_text_pressed");
    graphic_design.classList.remove("our_services_rest_pressed");
    graphic_design_text.classList.remove("our_services_rest_text_pressed");
    online_support.classList.remove("our_services_rest_pressed");
    online_support_text.classList.remove("our_services_rest_text_pressed");
    app_design.classList.remove("our_services_rest_pressed");
    app_design_text.classList.remove("our_services_rest_text_pressed");
    online_marketing.classList.remove("our_services_rest_pressed");
    online_marketing_text.classList.remove("our_services_rest_text_pressed");
    seo_service.classList.remove("our_services_rest_pressed");
    seo_service_text.classList.remove("our_services_rest_text_pressed");

    our_services_info_img.style.backgroundImage = "url('./img/Image.jpg')";
    our_services_info_text.textContent = `
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
        deserunt mollit anim id est laborum.
    `;
}

graphic_design.onclick = () => { 
    web_design.classList.remove("our_services_rest_pressed");
    web_design_text.classList.remove("our_services_rest_text_pressed");
    graphic_design.classList.add("our_services_rest_pressed");
    graphic_design_text.classList.add("our_services_rest_text_pressed");
    online_support.classList.remove("our_services_rest_pressed");
    online_support_text.classList.remove("our_services_rest_text_pressed");
    app_design.classList.remove("our_services_rest_pressed");
    app_design_text.classList.remove("our_services_rest_text_pressed");
    online_marketing.classList.remove("our_services_rest_pressed");
    online_marketing_text.classList.remove("our_services_rest_text_pressed");
    seo_service.classList.remove("our_services_rest_pressed");
    seo_service_text.classList.remove("our_services_rest_text_pressed");

    our_services_info_img.style.backgroundImage = "url('./img/11111.jpg')";
    our_services_info_text.textContent = `
        After the shooting of Captain McCluskey, the police took revenge on all 
        five New York families. The Five Families War of 1946 had begun. But 
        Michael wasn't there. He was hiding thousands of miles away, in Sicily. 
        He was staying with Don Tommasino, a friend of his father's and a 
        Godfather himself. In the evenings, Michael sat in a huge garden filled 
        with flowers, drinking wine and hearing old stories about his family. 
        During the day, he walked in the Sicilian countryside, dressed in old 
        clothes. Two bodyguards, Fabrizio and Calo, went everywhere with 
        him, carrying guns over their shoulders.
    `;
}

online_support.onclick = () => { 
    // our_services_info_img.style.transition = "all 0.5s ease";
    web_design.classList.remove("our_services_rest_pressed");
    web_design_text.classList.remove("our_services_rest_text_pressed");
    graphic_design.classList.remove("our_services_rest_pressed");
    graphic_design_text.classList.remove("our_services_rest_text_pressed");
    online_support.classList.add("our_services_rest_pressed");
    online_support_text.classList.add("our_services_rest_text_pressed");
    app_design.classList.remove("our_services_rest_pressed");
    app_design_text.classList.remove("our_services_rest_text_pressed");
    online_marketing.classList.remove("our_services_rest_pressed");
    online_marketing_text.classList.remove("our_services_rest_text_pressed");
    seo_service.classList.remove("our_services_rest_pressed");
    seo_service_text.classList.remove("our_services_rest_text_pressed");

    our_services_info_img.style.backgroundImage = "url('./img/22222.jpg')";
    our_services_info_text.textContent = `
        Michael jumped to his feet, pulled the gun from his trousers, pointed it 
        straight at Sollozzo's head and fired. The bullet hit Sollozzo between the 
        eyes. McCluskey stared at Sollozzo in surprise, as if watching 
        something far away. He did not seem to realize his own danger. His fork 
        was half-way to his mouth. He was just beginning to understand what 
        was happening when Michael fired at him. The shot was bad. It hit 
        McCluskey in the throat. He dropped his fork, put his hands to his neck 
        and began to cough up food and blood. Very carefully, very coolly, 
        Michael tired the next bullet straight into the policeman's brain. 
        McCluskey stared at Michael for a second then fell forward, his head 
        hitting the table with a crash.
    `;
}

app_design.onclick = () => { 
    // our_services_info_img.style.transition = "all 0.5s ease";
    web_design.classList.remove("our_services_rest_pressed");
    web_design_text.classList.remove("our_services_rest_text_pressed");
    graphic_design.classList.remove("our_services_rest_pressed");
    graphic_design_text.classList.remove("our_services_rest_text_pressed");
    online_support.classList.remove("our_services_rest_pressed");
    online_support_text.classList.remove("our_services_rest_text_pressed");
    app_design.classList.add("our_services_rest_pressed");
    app_design_text.classList.add("our_services_rest_text_pressed");
    online_marketing.classList.remove("our_services_rest_pressed");
    online_marketing_text.classList.remove("our_services_rest_text_pressed");
    seo_service.classList.remove("our_services_rest_pressed");
    seo_service_text.classList.remove("our_services_rest_text_pressed");

    our_services_info_img.style.backgroundImage = "url('./img/33333.jpg')";
    our_services_info_text.textContent = `
        Sollozzo, thought for a moment, then said: 'I have to go 'to the 
        bathroom. Is that OK?' 
        'No problem,' said McCluskey. 
        But Sollozzo didn't like it. When Michael stood up, he stopped him and 
        searched him very carefully. Finally satisfied that Michael wasn't 
        carrying a gun, he sat down again. 'Don't take too long,' he said, staring 
        at Michael moodily. 
        Michael found the gun hidden in the toilet. Clemenza had done his job 
        well. He pushed the gun into the top of his trousers, buttoned his jacket, 
        took a few deep breaths to calm himself down, and returned to the 
        restaurant. 
    `;
}

online_marketing.onclick = () => { 
    // our_services_info_img.style.transition = "all 0.5s ease";
    web_design.classList.remove("our_services_rest_pressed");
    web_design_text.classList.remove("our_services_rest_text_pressed");
    graphic_design.classList.remove("our_services_rest_pressed");
    graphic_design_text.classList.remove("our_services_rest_text_pressed");
    online_support.classList.remove("our_services_rest_pressed");
    online_support_text.classList.remove("our_services_rest_text_pressed");
    app_design.classList.remove("our_services_rest_pressed");
    app_design_text.classList.remove("our_services_rest_text_pressed");
    online_marketing.classList.add("our_services_rest_pressed");
    online_marketing_text.classList.add("our_services_rest_text_pressed");
    seo_service.classList.remove("our_services_rest_pressed");
    seo_service_text.classList.remove("our_services_rest_text_pressed");

    our_services_info_img.style.backgroundImage = "url('./img/44444.jpg')";
    our_services_info_text.textContent = `
        When the waiter had poured wine into their glasses, Sollozzo began to 
        talk to Michael in Italian. 'I have great respect for your father,' he said. 
        'What happened between him and me is just business. His thinking is 
        old-fashioned. Let's forget these disagreements. I want peace.' 
        Michael tried to reply in Italian, but he couldn't think of the words. So 
        he spoke English instead. 'You must promise me that no one will try to 
        kill my father again.' 
        Sollozzo looked at Michael in wide-eyed innocence. 'You think too 
        much of me,' he said. 'I'm the one in danger, not your father. I'm not as 
        clever as you think. All I want is peace.' 
        Michael looked at McCluskey. 
    `;
}

seo_service.onclick = () => { 
    // our_services_info_img.style.transition = "all 0.5s ease";
    web_design.classList.remove("our_services_rest_pressed");
    web_design_text.classList.remove("our_services_rest_text_pressed");
    graphic_design.classList.remove("our_services_rest_pressed");
    graphic_design_text.classList.remove("our_services_rest_text_pressed");
    online_support.classList.remove("our_services_rest_pressed");
    online_support_text.classList.remove("our_services_rest_text_pressed");
    app_design.classList.remove("our_services_rest_pressed");
    app_design_text.classList.remove("our_services_rest_text_pressed");
    online_marketing.classList.remove("our_services_rest_pressed");
    online_marketing_text.classList.remove("our_services_rest_text_pressed");
    seo_service.classList.add("our_services_rest_pressed");
    seo_service_text.classList.add("our_services_rest_text_pressed");

    our_services_info_img.style.backgroundImage = "url('./img/55555.jpg')";
    our_services_info_text.textContent = `
        'Don't worry,' Sollozzo said warmly. 'He'll be safe. I promise. But please 
        keep an open mind when we talk. I hope you're not a hothead like your 
        brother, Sonny. You can't talk business with him.' 
        Just then, McCluskey moved forward in his seat and offered Michael his 
        hand. 'You're a good boy,' he said in a strong, friendly voice. 'I'm sorry 
        about the other night, Mike. Nothing personal, I hope. I'm getting too 
        old for my job, I guess.' 
        Without turning round, Michael shook the policeman's hand over his 
        shoulder. 
        'And now I'm afraid I've got to search you,' McCluskey said. 'So turn 
        round please, on your knees.
    `;
}
// end 1_JS
// start 2_JS
our_amazing_btn.onclick = () => { 
    // let div_container;
    // let our_amazing_imgs2 = document.createElement("div");
    // let img1, img2, img3;
   

    // our_amazing_imgs2.classList.add("our_amazing_imgs");

    // for (let i = 0; i <= 3; i++) { 
    //     div_container = document.createElement("div");
    //     div_container.classList.add("our_amazing_imgs_col");
        
    //     img1 = document.createElement("img");
    //     img2 = document.createElement("img");
    //     img3 = document.createElement("img");
    //     img1.src = `./img/amazing${i}2.jpg`;
    //     img2.src = `./img/amazing${i}1.jpg`;
    //     img3.src = `./img/amazing${i}3.jpg`;
    //     img1.classList.add(`our_amazing_imgs_item`, `a_img${(3*i) + 1 + 12}`);
    //     img2.classList.add(`our_amazing_imgs_item`, `a_img${(3*i) + 2 + 12}`);
    //     img3.classList.add(`our_amazing_imgs_item`, `a_img${(3*i) + 3 + 12}`);
    //     div_container.append(img1);
    //     div_container.append(img2);
    //     div_container.append(img3);

    //     our_amazing_imgs2.append(div_container);
        
    // }

    our_amazing2.classList.remove("hidden");
    container3.style.height = "1830px";
    our_amazing.style.height = "949px";
    // our_amazing_imgs.after(our_amazing_imgs2);
    our_amazing_btn.remove();
}
// end 2_JS

// start 3_JS
our_amazing_menu_btn1.onclick = () => { 
    
    our_amazing_menu_btn1.classList.add("a_menu_pressed");
    our_amazing_menu_btn1_text.classList.add("a_menu_text_pressed");
    our_amazing_menu_btn2.classList.remove("a_menu_pressed");
    our_amazing_menu_btn2_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn3.classList.remove("a_menu_pressed");
    our_amazing_menu_btn3_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn4.classList.remove("a_menu_pressed");
    our_amazing_menu_btn4_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn5.classList.remove("a_menu_pressed");
    our_amazing_menu_btn5_text.classList.remove("a_menu_text_pressed");

    for (let i = 1; i <= 24; i++) { 
        if (document.querySelector(`.a_img${i}`)) { 
            document.querySelector(`.a_img${i}`).classList.remove("hidden");  
        }        
    }

}
our_amazing_menu_btn2.onclick = () => { 
    
    our_amazing_menu_btn1.classList.remove("a_menu_pressed");
    our_amazing_menu_btn1_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn2.classList.add("a_menu_pressed");
    our_amazing_menu_btn2_text.classList.add("a_menu_text_pressed");
    our_amazing_menu_btn3.classList.remove("a_menu_pressed");
    our_amazing_menu_btn3_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn4.classList.remove("a_menu_pressed");
    our_amazing_menu_btn4_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn5.classList.remove("a_menu_pressed");
    our_amazing_menu_btn5_text.classList.remove("a_menu_text_pressed");

    for (let i = 1; i <= 24; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.add("hidden");
        }
    }
    for (let i = 1; i <= 3; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.remove("hidden");
        }
        if (document.querySelector(`.a_img${i + 12}`)) {
            document.querySelector(`.a_img${i + 12}`).classList.remove("hidden");
        }
    }
    
}
our_amazing_menu_btn3.onclick = () => { 
    
    our_amazing_menu_btn1.classList.remove("a_menu_pressed");
    our_amazing_menu_btn1_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn2.classList.remove("a_menu_pressed");
    our_amazing_menu_btn2_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn3.classList.add("a_menu_pressed");
    our_amazing_menu_btn3_text.classList.add("a_menu_text_pressed");
    our_amazing_menu_btn4.classList.remove("a_menu_pressed");
    our_amazing_menu_btn4_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn5.classList.remove("a_menu_pressed");
    our_amazing_menu_btn5_text.classList.remove("a_menu_text_pressed");
    
    for (let i = 1; i <= 24; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.add("hidden");
        }
    }
    for (let i = 4; i <= 6; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.remove("hidden");
        }
        if (document.querySelector(`.a_img${i + 12}`)) {
            document.querySelector(`.a_img${i + 12}`).classList.remove("hidden");
        }
    }
}
our_amazing_menu_btn4.onclick = () => { 
    
    our_amazing_menu_btn1.classList.remove("a_menu_pressed");
    our_amazing_menu_btn1_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn2.classList.remove("a_menu_pressed");
    our_amazing_menu_btn2_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn3.classList.remove("a_menu_pressed");
    our_amazing_menu_btn3_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn4.classList.add("a_menu_pressed");
    our_amazing_menu_btn4_text.classList.add("a_menu_text_pressed");
    our_amazing_menu_btn5.classList.remove("a_menu_pressed");
    our_amazing_menu_btn5_text.classList.remove("a_menu_text_pressed");
    
    for (let i = 1; i <= 24; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.add("hidden");
        }
    }
    for (let i = 7; i <= 9; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.remove("hidden");
        }
        if (document.querySelector(`.a_img${i + 12}`)) {
            document.querySelector(`.a_img${i + 12}`).classList.remove("hidden");
        }
    }
}
our_amazing_menu_btn5.onclick = () => { 
    
    our_amazing_menu_btn1.classList.remove("a_menu_pressed");
    our_amazing_menu_btn1_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn2.classList.remove("a_menu_pressed");
    our_amazing_menu_btn2_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn3.classList.remove("a_menu_pressed");
    our_amazing_menu_btn3_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn4.classList.remove("a_menu_pressed");
    our_amazing_menu_btn4_text.classList.remove("a_menu_text_pressed");
    our_amazing_menu_btn5.classList.add("a_menu_pressed");
    our_amazing_menu_btn5_text.classList.add("a_menu_text_pressed");
    
    for (let i = 1; i <= 24; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.add("hidden");
        }
    }
    for (let i = 10; i <= 12; i++) { 
        if (document.querySelector(`.a_img${i}`)) {
            document.querySelector(`.a_img${i}`).classList.remove("hidden");
        }
        if (document.querySelector(`.a_img${i + 12}`)) {
            document.querySelector(`.a_img${i + 12}`).classList.remove("hidden");
        }
    }
}
// end 3_JS

// start 4_JS
let step_left = 1;
let move_left = 0;
let step_right = 1;
let left_end = 0;
let right_end = 0;
slider_photos_btnleft.onclick = () => { 
    right_end = 0;

    if (step_left <= 4 && left_end === 0) { 
        // let x1 = slider_photos_1.getBoundingClientRect().left
        // console.log(slider_photos_1.getBoundingClientRect().left);
        // console.log(slider_photos_1.style);
        // slider_photos_1.style.left = "90px"; 
        // if (step_left > 4) { 
        //     step_left = 4;
        // }
        console.log("step_left:", step_left, "step_right:", step_right);
        // console.log(window.getComputedStyle(slider_photos_1).backgroundImage);

        slider_photos_1.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_2.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_3.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_4.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_5.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_6.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_7.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_8.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_9.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_10.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_11.style.transform = `translate(${-98*step_left}px)`;
        slider_photos_12.style.transform = `translate(${-98 * step_left}px)`;

        slider_name.textContent = slider_names[6 + step_left];
        slider_text.textContent = slider_texts[6 + step_left];
        slider_position.textContent = slider_positions[6 + step_left];

        if (6 + step_left === 2) { changePhoto(slider_photos_11); }
        if (6 + step_left === 3) { changePhoto(slider_photos_12); }
        if (6 + step_left === 4) { changePhoto(slider_photos_1); }
        if (6 + step_left === 5) { changePhoto(slider_photos_2); }
        if (6 + step_left === 6) { changePhoto(slider_photos_3); }
        if (6 + step_left === 7) { changePhoto(slider_photos_4); }
        if (6 + step_left === 8) { changePhoto(slider_photos_5); }
        if (6 + step_left === 9) { changePhoto(slider_photos_6); }
        if (6 + step_left === 10) { changePhoto(slider_photos_7); }
        if (6 + step_left === 11) { changePhoto(slider_photos_8); }
        if (6 + step_left === 12) { changePhoto(slider_photos_9);}
        if (6 + step_left === 13) { changePhoto(slider_photos_10); }
        if (6 + step_left === 14) { changePhoto(slider_photos_11); }
        if (6 + step_left === 15) { changePhoto(slider_photos_12);}
        
        


        // changePhoto(`slider_photos_${6 + step_left}`);
        left_end = 1;
        if (step_left < 4) { 
            step_left++;
            step_right--;
            left_end = 0;
        }
        
        // step_left++;
                            // move_left += 98 * step_left;
        // step_right--;      
                            // console.log("step_left:", step_left, "step_right:", step_right);
                            // console.log(slider_photos_1.getBoundingClientRect().left);
    }
}
slider_photos_btnright.onclick = () => { 

    left_end = 0;
    // if (step_left > 4) { 
    //         step_left = 4;
    // }
    console.log("step_left:", step_left, "step_right:", step_right);

    if (step_right <= 4 && right_end === 0) {
        slider_photos_1.style.transform = `translate(${98*step_right}px)`;
        slider_photos_2.style.transform = `translate(${98*step_right}px)`;
        slider_photos_3.style.transform = `translate(${98*step_right}px)`;
        slider_photos_4.style.transform = `translate(${98*step_right}px)`;
        slider_photos_5.style.transform = `translate(${98*step_right}px)`;
        slider_photos_6.style.transform = `translate(${98*step_right}px)`;
        slider_photos_7.style.transform = `translate(${98*step_right}px)`;
        slider_photos_8.style.transform = `translate(${98*step_right}px)`;
        slider_photos_9.style.transform = `translate(${98*step_right}px)`;
        slider_photos_10.style.transform = `translate(${98*step_right}px)`;
        slider_photos_11.style.transform = `translate(${98*step_right}px)`;
        slider_photos_12.style.transform = `translate(${98 * step_right}px)`;

        slider_name.textContent = slider_names[6 - step_right];
        slider_text.textContent = slider_texts[6 - step_right];
        slider_position.textContent = slider_positions[6 - step_right];

        if (6 - step_right === 2) { changePhoto(slider_photos_11); }
        if (6 - step_right === 3) { changePhoto(slider_photos_12); }
        if (6 - step_right === 4) { changePhoto(slider_photos_1); }
        if (6 - step_right === 5) { changePhoto(slider_photos_2); }
        if (6 - step_right === 6) { changePhoto(slider_photos_3); }
        if (6 - step_right === 7) { changePhoto(slider_photos_4); }
        if (6 - step_right === 8) { changePhoto(slider_photos_5); }
        if (6 - step_right === 9) { changePhoto(slider_photos_6); }
        if (6 - step_right === 10) { changePhoto(slider_photos_7); }
        if (6 - step_right === 11) { changePhoto(slider_photos_8); }
        if (6 - step_right === 12) { changePhoto(slider_photos_9);}
        if (6 - step_right === 13) { changePhoto(slider_photos_10); }
        if (6 - step_right === 14) { changePhoto(slider_photos_11); }
        if (6 - step_right === 15) { changePhoto(slider_photos_12); }
        
        right_end = 1;
        if (right_end < 4) { 
            step_right++;
            step_left--;
            right_end = 0;
        }

        // step_right++;
        // step_left--;
                                // console.log("step_left:", step_left, "step_right:", step_right);
    } 
}

// let x;
// slider_photos_1.onclick = () => {

//     let str = window.getComputedStyle(slider_photos_1).backgroundImage;
//     console.log(str);
//     let index = str.indexOf("/img/");
//     let path = str.slice(index, str.length - 2);
//     path = path.replace("%20", "\\ ");
//     console.log(path);

//     // "url('./img/33333.jpg')";

//     slider_main_photo.style.animationName = "hide";
//         let x = setTimeout(
//         () => {
//                 slider_main_photo.style.animationName = "show";
//                 slider_main_photo.style.backgroundImage = `url('.${path}')`;
//                 clearTimeout(x);
//         },
//         150
//     );
// }

slider_photos_1.onclick = () => {
    changePhoto(slider_photos_1);
}
slider_photos_2.onclick = () => {
    changePhoto(slider_photos_2);
}
slider_photos_3.onclick = () => {
    changePhoto(slider_photos_3);
}
slider_photos_4.onclick = () => {
    changePhoto(slider_photos_4);
}
slider_photos_5.onclick = () => {
    changePhoto(slider_photos_5);
}
slider_photos_6.onclick = () => {
    changePhoto(slider_photos_6);
}
slider_photos_7.onclick = () => {
    changePhoto(slider_photos_7);
}
slider_photos_8.onclick = () => {
    changePhoto(slider_photos_8);
}
slider_photos_9.onclick = () => {
    changePhoto(slider_photos_9);
}
slider_photos_10.onclick = () => {
    changePhoto(slider_photos_10);
}
slider_photos_11.onclick = () => {
    changePhoto(slider_photos_11);
}
slider_photos_12.onclick = () => {
    changePhoto(slider_photos_12);
}


function changePhoto(element) {
let str = window.getComputedStyle(element).backgroundImage;
    // console.log(str);
    let index = str.indexOf("/img/");
    let path = str.slice(index, str.length - 2);
    path = path.replace("%20", "\\ ");
    // console.log(path);
    // "url('./img/33333.jpg')";
    slider_main_photo.style.animationName = "hide";

    console.log(element.classList[0]);
    let photo_class = element.classList[0];
    let index_p = photo_class.lastIndexOf("_");
    let photo_class_num = photo_class.slice(index_p+1, photo_class.length);
    console.log(index_p, photo_class_num);

    slider_name.textContent = slider_names[+photo_class_num-1];
    slider_text.textContent = slider_texts[+photo_class_num-1];
    slider_position.textContent = slider_positions[+photo_class_num-1];

    // slider_name.textContent = slider_names[6 - step_right];
    // slider_text.textContent = slider_texts[6 - step_right];
    // slider_position.textContent = slider_positions[6 - step_right];

        let x = setTimeout(
        () => { 
                slider_main_photo.style.animationName = "show";
                slider_main_photo.style.backgroundImage = `url('.${path}')`;
                clearTimeout(x);
        }, 
        150
    );    
}

// end 4_JStransform